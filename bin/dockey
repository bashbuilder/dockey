#!/bin/bash

[[ -z "$DEBUG" ]] || set -x
[[ -z "$DEBUG" ]] || BASH_ARGS=" -x "

BASH="bash -c -l $BASH_ARGS"

[ -z "$DOCKEY_NAME" ] && DOCKEY_NAME=little-dockey
[ -z "$LXC_PATH" ]    &&    LXC_PATH="/var/lib/lxc/"
[ -z "$LXC_HOST" ]    &&    LXC_HOST="10.0.3.1"

action="$1"
shift

function package {
  name=$1
  shift
  eval "DOCKEY_PACKAGE_$name=\"$*\""
}

function serve_http {
  old_pwd="$(pwd)"
  cd $(dirname $0)/../rpms
  python -m SimpleHTTPServer 7950 &
  DOCKEY_RPMS_PID="$!"
  cd "$old_pwd"
}

function stop_http {
  kill -9 $DOCKEY_RPMS_PID
}

function dockey-install {
  serve_http
  . $(dirname $0)/../config/packages
  packages="DOCKEY_PACKAGE_$1"
  packages="$(eval echo \$"$packages")"
  sudo ${LXC_PREFIX}lxc-attach -n $DOCKEY_NAME -- $BASH "yum -y install $packages"
  type -t "after-$1" | grep -q "function" && after-$1
  dockey-refresh
  stop_http
}

function dockey-create {
  sudo ${LXC_PREFIX}lxc-create -n $DOCKEY_NAME -t centos
  dockey-setup
  dockey-refresh
}

function dockey-refresh {
  sudo rm $LXC_PATH/$DOCKEY_NAME/rootfs/sbin/start_udev 2>/dev/null
}

function dockey-setup {
  sudo rm -r $LXC_PATH/$DOCKEY_NAME/rootfs/etc/yum.repos.d/dockey 2>/dev/null
  sudo cp -r $(dirname $0)/../config/yum $LXC_PATH/$DOCKEY_NAME/rootfs/etc/yum.repos.d/dockey
  cd $LXC_PATH/$DOCKEY_NAME/rootfs/etc/yum.repos.d
  sudo ln -s dockey/* ./
}

function dockey-start {
  dockey-refresh
  sudo ${LXC_PREFIX}lxc-start -d -n $DOCKEY_NAME
}

function dockey-stop {
  dockey-refresh
  dockey-run shutdown -P 0
}

function dockey-reboot {
  dockey-refresh
  dockey-run reboot
}

function dockey-run {
  sudo ${LXC_PREFIX}lxc-attach -n $DOCKEY_NAME -- $BASH "$*"
}

function dockey-update {
  cd $(dirname $0)/..
  git fetch
  git checkout origin/master
  pwd
  . config/local
  dockey-setup
}

function dockey-autostart {
  if [[ "$1" = "on" ]]; then
    sudo ln -s $LXC_PATH/$DOCKEY_NAME/config /etc/lxc/auto/$DOCKEY_NAME
  else
    sudo rm /etc/lxc/auto/$DOCKEY_NAME
  fi
}

function dockey-dhcp {
  echo | dockey-run tee /etc/sysconfig/network-scripts/ifcfg-eth0 <<HERE
DEVICE=eth0
BOOTPROTO=dhcp
ONBOOT=yes
TYPE=Ethernet
USERCTL=yes
PEERDNS=yes
IPV6INIT=no
PERSISTENT_DHCLIENT=yes
DHCP_HOSTNAME="$DOCKEY_NAME"
HERE
  dockey-reboot
}

function dockey-mountin {
  host_path=$1
  in_path=$2
  sudo mkdir -p $LXC_PATH/$DOCKEY_NAME/rootfs${in_path}
  echo "lxc.mount.entry = $host_path ${LXC_PATH}${DOCKEY_NAME}/rootfs${in_path} none bind 0 0" | sudo tee -a ${LXC_PATH}${DOCKEY_NAME}/config
  dockey-stop
  sleep 5
  dockey-start
}

dockey-$action $*

